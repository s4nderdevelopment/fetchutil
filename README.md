# FetchUtil

FetchUtil is a simple wrapper library for fetch. 

## How to use

Use `npm install git+https://gitlab.com/s4development/fetchutil.git` to install the dependency.
Or if you prefer using SSH, use `npm install git+ssh://git@gitlab.com:s4development/fetchutil.git`

## Example

```js
import FetchUtil from "fetchutil/dist/index.js";

const API_SAMPLE_URL = "http://localhost:8080";

export class APISample extends FetchUtil {
    constructor(tokenProvider){
        super(API_SAMPLE_URL, tokenProvider);

        this.on(401, () => {
            window.localStorage.removeItem("token");
        });
    }

    users = {
        getAll: () => this.get("/users"),
        getById: (id) => this.get("/users/" + id),
        create: (customer) => this.post("/users", customer),
        update: (id, customer) => this.put("/users/" + id, customer)
    }
}

let mySampleAPI = new APISample(() => window.localStorage.getItem("token"));

mySampleAPI.users.getAll().then((response) => {
    console.log(response);
}, (error) => {
    console.error(error);
});
```

## TODO's

- [ ] Add to npm registry.
