export default class FetchUtil {
    private origin: string;
    private tokenProvider: () => string|null;

    private unknownErrorMessage: string = "Unknown error";

    private callbacks: any = {
        
    };

    constructor(origin: string, tokenProvider: () => string|null = () => null){
        this.origin = origin;
        this.tokenProvider = tokenProvider;
    }

    public setUnknownErrorMessage(unknownErrorMessage: string){
        this.unknownErrorMessage = unknownErrorMessage;
    }

    private request(path: string, settings: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const token = this.tokenProvider();
            if(token != null && token.length > 0){
                settings.headers["Authorization"] = "Bearer " + token;
            }
            fetch(this.origin + path, settings).then((response) => {
                let callbacks = this.callbacks[response.status];

                if(Array.isArray(callbacks)){
                    for(let callback of callbacks){
                        callback();
                    }
                }

                if(response.status >= 200 && response.status < 300){
                    if(response.status == 200){
                        response.json().then(resolve).catch(() => resolve(null));
                    }else if(response.status == 201){
                        let location = response.headers.get("Location");
                        let idIdx = location.lastIndexOf("/") + 1;
                        if(idIdx > 0){
                            resolve({
                                id: parseInt(location.substring(idIdx))
                            });
                        }else{
                            response.json().then(resolve).catch(() => resolve(null));
                        }
                    }else{
                        resolve(null);
                    }
                }else if(response.status >= 400 && response.status < 500){
                    if(response.status == 400){
                        response.json().then(reject).catch(() => reject(null));
                    }else if(response.status == 401){
                        response.json().then(reject).catch(() => reject(null));
                    }else if(response.status == 403){
                        response.json().then(reject).catch(() => reject(null));
                    }else if(response.status == 404){
                        response.json().then(reject).catch(() => reject(null));
                    }else if(response.status == 409){
                        response.json().then(reject).catch(() => reject(null));
                    }else{
                        reject({
                            reason: this.unknownErrorMessage
                        });
                    }
                }else{
                    reject({
                        reason: this.unknownErrorMessage
                    });
                }
            }).catch(() => {
                reject({
                    reason: this.unknownErrorMessage
                });
            });
        });
    }

    public get(path: string){
        return this.request(path, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow'
        });
    }
    
    public post(path: string, body: any){
        return this.request(path, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    }
    
    public put(path: string, body: any){
        return this.request(path, {
            method: 'PUT',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    }
    
    public patch(path: string, body: any){
        return this.request(path, {
            method: 'PATCH',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    }
    
    public del(path: string){
        return this.request(path, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow'
        });
    }

    public on(statusCode: number, callback: () => any){
        if(!Array.isArray(this.callbacks[statusCode])){
            this.callbacks[statusCode] = [];
        }
        this.callbacks[statusCode].push(callback);
    }
}
