export default class FetchUtil {
    private origin;
    private tokenProvider;
    private unknownErrorMessage;
    private callbacks;
    constructor(origin: string, tokenProvider?: () => string | null);
    setUnknownErrorMessage(unknownErrorMessage: string): void;
    private request;
    get(path: string): Promise<any>;
    post(path: string, body: any): Promise<any>;
    put(path: string, body: any): Promise<any>;
    patch(path: string, body: any): Promise<any>;
    del(path: string): Promise<any>;
    on(statusCode: number, callback: () => any): void;
}
