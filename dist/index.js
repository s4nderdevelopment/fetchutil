var FetchUtil = /** @class */ (function () {
    function FetchUtil(origin, tokenProvider) {
        if (tokenProvider === void 0) { tokenProvider = function () { return null; }; }
        this.unknownErrorMessage = "Unknown error";
        this.callbacks = {};
        this.origin = origin;
        this.tokenProvider = tokenProvider;
    }
    FetchUtil.prototype.setUnknownErrorMessage = function (unknownErrorMessage) {
        this.unknownErrorMessage = unknownErrorMessage;
    };
    FetchUtil.prototype.request = function (path, settings) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var token = _this.tokenProvider();
            if (token != null && token.length > 0) {
                settings.headers["Authorization"] = "Bearer " + token;
            }
            fetch(_this.origin + path, settings).then(function (response) {
                var callbacks = _this.callbacks[response.status];
                if (Array.isArray(callbacks)) {
                    for (var _i = 0, callbacks_1 = callbacks; _i < callbacks_1.length; _i++) {
                        var callback = callbacks_1[_i];
                        callback();
                    }
                }
                if (response.status >= 200 && response.status < 300) {
                    if (response.status == 200) {
                        response.json().then(resolve)["catch"](function () { return resolve(null); });
                    }
                    else if (response.status == 201) {
                        var location_1 = response.headers.get("Location");
                        var idIdx = location_1.lastIndexOf("/") + 1;
                        if (idIdx > 0) {
                            resolve({
                                id: parseInt(location_1.substring(idIdx))
                            });
                        }
                        else {
                            response.json().then(resolve)["catch"](function () { return resolve(null); });
                        }
                    }
                    else {
                        resolve(null);
                    }
                }
                else if (response.status >= 400 && response.status < 500) {
                    if (response.status == 400) {
                        response.json().then(reject)["catch"](function () { return reject(null); });
                    }
                    else if (response.status == 401) {
                        response.json().then(reject)["catch"](function () { return reject(null); });
                    }
                    else if (response.status == 403) {
                        response.json().then(reject)["catch"](function () { return reject(null); });
                    }
                    else if (response.status == 404) {
                        response.json().then(reject)["catch"](function () { return reject(null); });
                    }
                    else if (response.status == 409) {
                        response.json().then(reject)["catch"](function () { return reject(null); });
                    }
                    else {
                        reject({
                            reason: _this.unknownErrorMessage
                        });
                    }
                }
                else {
                    reject({
                        reason: _this.unknownErrorMessage
                    });
                }
            })["catch"](function () {
                reject({
                    reason: _this.unknownErrorMessage
                });
            });
        });
    };
    FetchUtil.prototype.get = function (path) {
        return this.request(path, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow'
        });
    };
    FetchUtil.prototype.post = function (path, body) {
        return this.request(path, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    };
    FetchUtil.prototype.put = function (path, body) {
        return this.request(path, {
            method: 'PUT',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    };
    FetchUtil.prototype.patch = function (path, body) {
        return this.request(path, {
            method: 'PATCH',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: body instanceof Blob ? body : JSON.stringify(body)
        });
    };
    FetchUtil.prototype.del = function (path) {
        return this.request(path, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow'
        });
    };
    FetchUtil.prototype.on = function (statusCode, callback) {
        if (!Array.isArray(this.callbacks[statusCode])) {
            this.callbacks[statusCode] = [];
        }
        this.callbacks[statusCode].push(callback);
    };
    return FetchUtil;
}());
export default FetchUtil;
